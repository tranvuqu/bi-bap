## _Setup_

Install requirements from requirements.txt file. 

Before launching the application, configure settings in settings.py file.

If no email backend is set up, the verification emails(registration, change email) cannot be sent via email backend. 
Setting variable SEND_EMAILS to False will print the email messages to the console instead.

Run migrations if a new database is used.
