from django.conf.urls import url
from bi_bap.home.views import *


app_name = 'home'
urlpatterns = [
    url(r'^$', HomeWeeklyView.as_view(), name='home_weekly'),
    url(r'^monthly/$', HomeMonthlyView.as_view(), name='home_monthly'),

]