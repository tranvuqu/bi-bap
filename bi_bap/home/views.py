import calendar
from datetime import date, timedelta, datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView

from bi_bap.activities.models import Activity, ActivityInstance
from bi_bap.settings import DATE_INPUT_FORMAT


def date_range(start_date, end_date):
    dates = []
    for n in range(int((end_date - start_date).days) + 1):
        dates.append(start_date + timedelta(n))
    return dates


def get_activity_table_data(user, date_from, date_to):
    """
    Function creates a dictionary of dictionaries to represent a 2D table of activity instances.
    """
    activities = Activity.objects.filter(created_by=user).order_by("name")

    activity_dates_dict = {}
    if activities:
        for activity in activities:
            date_activity_instance_dict = {}
            activity_instances = ActivityInstance.objects.filter(activity=activity, date__gte=date_from, date__lte=date_to)
            # data for date columns
            for activity_instance in activity_instances:
                date_activity_instance_dict[activity_instance.date] = activity_instance

            # data for activity rows
            activity_dates_dict[activity] = date_activity_instance_dict
    return activity_dates_dict


class HomeWeeklyView(LoginRequiredMixin, TemplateView):
    template_name = "home_weekly.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if not ("date" in self.request.GET):
            date_to_show = date.today()
        else:
            try:
                date_to_show = datetime.strptime(str(self.request.GET["date"]), "%d/%m/%Y").date()
            except:
                date_to_show = date.today()

        date_from = date_to_show - timedelta(days=date_to_show.weekday())
        date_to = date_from + timedelta(days=6)
        dates = date_range(date_from, date_to)

        date_activity_dict = get_activity_table_data(self.request.user, date_from, date_to)

        context["date_from"] = date_from.strftime(DATE_INPUT_FORMAT)
        context["date_to"] = date_to.strftime(DATE_INPUT_FORMAT)
        context["date_to_show"] = date_to_show
        context["dates"] = dates
        context["date_activity_dict"] = date_activity_dict

        return context


class HomeMonthlyView(LoginRequiredMixin, TemplateView):
    template_name = "home_monthly.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if not ("date" in self.request.GET):
            date_to_show = date.today()
        else:
            try:
                date_to_show = datetime.strptime(str(self.request.GET["date"]), "%d/%m/%Y")
            except:
                date_to_show = date.today()

        last_day = calendar.monthrange(date_to_show.year, date_to_show.month)[1]
        date_from = datetime(date_to_show.year, date_to_show.month, 1)
        date_to = datetime(date_to_show.year, date_to_show.month, last_day)
        dates = date_range(date_from, date_to)

        date_activity_dict = get_activity_table_data(self.request.user, date_from, date_to)

        context["date_from"] = date_from.strftime(DATE_INPUT_FORMAT)
        context["date_to"] = date_to.strftime(DATE_INPUT_FORMAT)
        context["date_to_show"] = date_to_show
        context["dates"] = dates
        context["date_activity_dict"] = date_activity_dict

        return context
