from datetime import timedelta, datetime

from django import template

from bi_bap.settings import DATE_INPUT_FORMAT

register = template.Library()


@register.filter(name='is_today')
def is_today(date):
    try:
        return date.strftime(DATE_INPUT_FORMAT) == date.today().strftime(DATE_INPUT_FORMAT)
    except:
        return False


@register.filter(name='get_item')
def get_item(dictionary, key):
    return dictionary.get(key)


@register.filter(name='get_prev_week')
def get_prev_week(date):
    if date:
        return (date - timedelta(days=7)).strftime("%d/%m/%Y")
    else:
        return ""


@register.filter(name='get_next_week')
def get_next_week(date):
    if date:
        return (date + timedelta(days=7)).strftime("%d/%m/%Y")
    else:
        return ""


@register.filter(name='get_prev_month')
def get_prev_month(date):
    if date:
        return (date.replace(day=1) - timedelta(days=2)).replace(day=1).strftime("%d/%m/%Y")
    else:
        return ""


@register.filter(name='get_next_month')
def get_next_month(date):
    if date:
        return (date.replace(day=1) + timedelta(days=32)).replace(day=1).strftime("%d/%m/%Y")
    else:
        return ""


@register.filter(name='get_prev_year')
def get_prev_year(date):
    if date:
        return (datetime(date.year - 1, 1, 1)).strftime("%d/%m/%Y")
    else:
        return ""


@register.filter(name='get_next_year')
def get_next_year(date):
    if date:
        return (datetime(date.year + 1, 1, 1)).strftime("%d/%m/%Y")
    else:
        return ""
