from django.conf.urls import url

from bi_bap.rewards.views import RewardsIndexView

app_name = 'rewards'
urlpatterns = [
    url(r'^$', RewardsIndexView.as_view(), name='index_rewards'),

]