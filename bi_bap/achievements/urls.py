from django.conf.urls import url

from bi_bap.achievements.views import *

app_name = 'achievements'
urlpatterns = [
    url(r'^$', AchievementDetailView.as_view(), name='index_achievements'),
    url(r'^(?P<pk>[\w-]+)/$', AchievementDetailView.as_view(), name='achievement_detail'),

]