import datetime

from django.test import TestCase

# Create your tests here.
from bi_bap.account.models import User
from bi_bap.achievements.models import Achievement, NUMBER_OF_COMPLETION, HOT_STREAK
from bi_bap.achievements.views import create_num_of_completion_achievement, create_streak_achievement
from bi_bap.activities.models import Activity, ActivityInstance


class AchievementTestCase(TestCase):
    def setUp(self):
        user1 = User.objects.create(username="user1")
        Activity.objects.create(name="activity1", created_by=user1,
                                start_date=datetime.datetime(2022, 4, 1),
                                end_date=datetime.datetime(2022, 4, 30))

    def test_create_num_of_completion_achievement(self):
        total_instances = 30
        num_of_equal_parts = 4
        user1 = User.objects.get(username="user1")
        activity1 = Activity.objects.get(name="activity1")
        new_achievements = create_num_of_completion_achievement(activity1, user1, num_of_equal_parts, total_instances)
        self.assertEqual(len(new_achievements), num_of_equal_parts)
        self.assertEqual(new_achievements[0].goal, 8)
        self.assertEqual(new_achievements[1].goal, 15)
        self.assertEqual(new_achievements[2].goal, 22)
        self.assertEqual(new_achievements[3].goal, 30)

    def test_create_streak_achievement(self):
        total_instances = 30
        completion_percentage = 50
        user1 = User.objects.get(username="user1")
        activity1 = Activity.objects.get(name="activity1")
        new_achievements = create_streak_achievement(activity1, user1, completion_percentage, total_instances)
        self.assertEqual(len(new_achievements), 2)
        self.assertEqual(new_achievements[0].goal, 7)
        self.assertEqual(new_achievements[1].goal, 14)

    def test_evaluate_achievement(self):
        user1 = User.objects.get(username="user1")
        activity1 = Activity.objects.get(name="activity1")
        complete1 = ActivityInstance.objects.create(activity=activity1, date=datetime.datetime(2022, 4, 1), is_complete=True)
        not_complete1 = ActivityInstance.objects.create(activity=activity1, date=datetime.datetime(2022, 4, 2), is_complete=False)
        complete2 = ActivityInstance.objects.create(activity=activity1, date=datetime.datetime(2022, 4, 3), is_complete=True)
        complete3 = ActivityInstance.objects.create(activity=activity1, date=datetime.datetime(2022, 4, 4), is_complete=True)
        completion = Achievement.objects.create(activity=activity1, type=NUMBER_OF_COMPLETION, created_by=user1, progress=49, goal=50)

        completion.evaluate_achievement(not_complete1)
        self.assertEqual(completion.progress, 48)
        self.assertEqual(completion.is_complete, False)

        completion.evaluate_achievement(complete1)
        completion.evaluate_achievement(complete2)

        self.assertEqual(completion.progress, 50)
        self.assertEqual(completion.is_complete, True)

        completion.evaluate_achievement(complete3)

        self.assertEqual(completion.progress, 50)
        self.assertEqual(completion.is_complete, True)

    def test_evaluate_streak(self):
        user1 = User.objects.get(username="user1")
        activity1 = Activity.objects.get(name="activity1")
        complete1 = ActivityInstance.objects.create(activity=activity1, date=datetime.datetime(2022, 4, 1), is_complete=True)
        ActivityInstance.objects.create(activity=activity1, date=datetime.datetime(2022, 4, 2), is_complete=False)
        ActivityInstance.objects.create(activity=activity1, date=datetime.datetime(2022, 4, 3), is_complete=True)
        ActivityInstance.objects.create(activity=activity1, date=datetime.datetime(2022, 4, 4), is_complete=True)
        streak = Achievement.objects.create(activity=activity1, type=HOT_STREAK, created_by=user1, goal=5)
        streak.evaluate_achievement(complete1)
        self.assertEqual(streak.progress, 2)
        self.assertEqual(streak.is_complete, False)
