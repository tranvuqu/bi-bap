from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404

# Create your views here.
from django.views.generic import TemplateView, UpdateView

from bi_bap.achievements.models import Achievement, NUMBER_OF_COMPLETION, HOT_STREAK
from bi_bap.activities.models import Activity


def create_num_of_completion_achievement(new_activity, user, num_of_equal_parts, total_instances):
    """
    Function creates new completion achievements.
    The goal of each achievement is increments of total number of instances divided by num_of_equal_parts.
    """
    new_achievements = []
    for i in range(1, num_of_equal_parts + 1):
        goal = round((total_instances/num_of_equal_parts)*i)
        new_achievements.append(
            Achievement(activity=new_activity, created_by=user, type=NUMBER_OF_COMPLETION, progress=0, goal=goal, picture="completion.png"))

    return new_achievements


def create_streak_achievement(new_activity, user, completion_percentage, total_instances):
    """
    Function creates new streak achievements.
    Completion percentage sets the max number of instances to be counted as a streak.
    The goal of each streak achievement is calculated by increments of 7.
    """
    new_achievements = []
    stop = round((total_instances/100)*completion_percentage)
    for i in range(7, stop, 7):
        new_achievements.append(
            Achievement(activity=new_activity, created_by=user, type=HOT_STREAK, progress=0, goal=i, picture="streak.png"))

    return new_achievements


class AchievementDetailView(LoginRequiredMixin, TemplateView):
    template_name = "achievement_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            kwargs_pk = self.kwargs['pk']
            current_activity = get_object_or_404(Activity, pk=kwargs_pk, created_by=self.request.user)
            achievements = Achievement.objects.filter(activity=current_activity, created_by=self.request.user).order_by('is_complete')
        except KeyError:
            achievements = Achievement.objects.filter(created_by=self.request.user).order_by('is_complete')
            current_activity = None

        activities = Activity.objects.filter(created_by=self.request.user)
        context['current_activity'] = current_activity
        context['achievements'] = achievements
        context['activities'] = activities
        return context
