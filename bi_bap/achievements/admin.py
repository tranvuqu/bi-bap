from django.contrib import admin

# Register your models here.
from bi_bap.achievements.models import Achievement

admin.site.register(Achievement)
