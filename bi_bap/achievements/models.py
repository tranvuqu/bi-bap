from django.db import models

# Create your models here.
from bi_bap.account.models import User, Notification
from bi_bap.activities.models import Activity
from bi_bap.settings import IMG_DIR

NUMBER_OF_COMPLETION = 0
HOT_STREAK = 1

ACHIEVEMENT_TYPES = (
    (NUMBER_OF_COMPLETION, "Number of Completion"),
    (HOT_STREAK, "Hot Streak"),
)


class Achievement(models.Model):
    activity = models.ForeignKey(Activity, verbose_name="Activity", on_delete=models.CASCADE, related_name="achievements")
    type = models.IntegerField(default=0, choices=ACHIEVEMENT_TYPES)
    created_by = models.ForeignKey(User, verbose_name="User", on_delete=models.CASCADE, related_name="achievements")
    progress = models.IntegerField(verbose_name="Progress", default=0)
    goal = models.IntegerField(verbose_name="Goal", default=0)
    is_complete = models.BooleanField("Complete", default=False)
    picture = models.FilePathField(path=IMG_DIR, default="completion.png")

    def calculate_progress(self):
        if self.progress == 0 or self.goal == 0:
            return 0
        else:
            return round(self.progress / (self.goal / 100))

    def evaluate_completion_achievement(self, activity_instance):
        if activity_instance.is_complete:
            self.progress = self.progress + 1
        else:
            self.progress = self.progress - 1

        if self.goal == self.progress:
            self.is_complete = True
            self.create_notification()
        self.save()

    def evaluate_streak_achievement(self):
        activity_instances = self.activity.activity_instances.all().order_by('date')
        streak = 0
        best_streak = 0
        for activity_instance in activity_instances:
            if activity_instance.is_complete:
                streak = streak + 1
                if streak > best_streak:
                    best_streak = streak
                if streak == self.goal:
                    self.is_complete = True
                    self.create_notification()
                    break
            else:
                streak = 0
        self.progress = best_streak
        self.save()

    def evaluate_achievement(self, activity_instance):
        if self.is_complete:
            return

        if self.type == NUMBER_OF_COMPLETION:
            return self.evaluate_completion_achievement(activity_instance)
        elif self.type == HOT_STREAK:
            return self.evaluate_streak_achievement()

    def create_notification(self):
        if self.type == NUMBER_OF_COMPLETION:
            Notification.objects.create(text=f"Completed {self.activity.name} {self.goal} times.", created_by=self.created_by)
        elif self.type == HOT_STREAK:
            Notification.objects.create(text=f"Completed {self.activity.name} {self.goal} times in a row.", created_by=self.created_by)
        else:
            return

    def __str__(self):
        if self.type == NUMBER_OF_COMPLETION:
            return f"Complete habit {self.goal} times"
        elif self.type == HOT_STREAK:
            return f"Complete habit {self.goal} times in a row"
        else:
            return ""


