from django.contrib.auth.tokens import PasswordResetTokenGenerator

account_token_generator = PasswordResetTokenGenerator()
