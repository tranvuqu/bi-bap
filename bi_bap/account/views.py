from datetime import date

from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import send_mail
from django.db import transaction
from django.http import HttpResponse
from django.shortcuts import redirect, get_object_or_404

# Create your views here.
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views import View
from django.views.generic import FormView
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView as AuthPasswordChangeView, \
    PasswordResetView, PasswordResetConfirmView

from bi_bap.account.forms import UserLoginForm, UserRegisterForm, ChangePasswordForm, UserChangeEmailForm, \
    ChangeProfilePicture, UserPasswordResetForm, UserSetPasswordForm
from bi_bap.account.models import User, Notification
from bi_bap.account.tokens import account_token_generator
from bi_bap.achievements.models import Achievement
from bi_bap.activities.models import ActivityInstance
from bi_bap.settings import EMAIL_HOST_USER, SEND_EMAILS


class AccountIndexView(LoginRequiredMixin, FormView):
    template_name = "index_account.html"
    form_class = ChangeProfilePicture
    success_url = reverse_lazy("account:index_account")

    def form_valid(self, form):
        form = self.form_class(self.request.POST, self.request.FILES, instance=self.request.user)
        form.save()
        self.request.user.profile_pic = form.cleaned_data['profile_pic']
        self.request.user.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # calculate completed achievements
        recent_achievements = Achievement.objects.filter(created_by=self.request.user)
        achievements_total = 0
        achievements_complete = 0
        recent_completed_achievements = []
        for achievement in recent_achievements:
            if achievement.is_complete:
                achievements_complete += 1
                recent_completed_achievements.append(achievement)
            achievements_total += 1

        # calculate completed activities for today
        instances = ActivityInstance.objects.filter(activity__created_by=self.request.user, date=date.today())
        instances_total = 0
        instances_complete = 0
        for instance in instances:
            if instance.is_complete:
                instances_complete += 1
            instances_total += 1

        context['achievements_total'] = achievements_total
        context['achievements_complete'] = achievements_complete
        context['instances_total'] = instances_total
        context['instances_complete'] = instances_complete
        context['recent_completed_achievements'] = recent_completed_achievements[:5]
        return context


class UserRegisterView(FormView):
    """
    View displays email change form and sends verification link to the entered email.
    """
    form_class = UserRegisterForm
    template_name = "register.html"
    success_url = reverse_lazy("account:register")

    def form_valid(self, form):
        try:
            with transaction.atomic():
                form = self.form_class(self.request.POST)
                user = form.save()
                current_site = get_current_site(self.request)
                subject = 'Confirm your email address'
                user.is_active = False
                user.signup_confirmation = False
                user.save()
                # load a template like get_template()
                # and calls its render() method immediately.
                message = render_to_string('activation_request_register.html', {
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    # method will generate a hash value with user related data
                    'token': account_token_generator.make_token(user),
                })
                messages.success(self.request, "Email has been sent. Check your email for further details.")

                if SEND_EMAILS:
                    user.email_user(subject, message, fail_silently=False)
                else:
                    print(message)

                return super().form_valid(form)

        except Exception as e:
            messages.warning(self.request,
                             "Error trying to send email. If problem persists, please contact staff members.")
            return super().form_invalid(form)


def activate_registration(request, uidb64, token):
    """
    Function checks the token sent during the registration process,
    if the token is valid then the email is verified and the registration process is finished,
    otherwise an error message is shown.
    """
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    # checking if the user exists and if the token is valid.
    if user is not None and account_token_generator.check_token(user, token):
        user.is_active = True
        user.signup_confirmation = True
        user.save()
        login(request, user)
        messages.success(request, "Welcome " + user.username + "!")
        return redirect('account:index_account')
    else:
        messages.warning(request,
                         "Link has expired.")
        return redirect('account:register')


class PasswordChangeView(LoginRequiredMixin, AuthPasswordChangeView):
    """
    View displays a form to change password
    """
    form_class = ChangePasswordForm
    success_url = reverse_lazy("account:index_account")
    template_name = "password_change.html"


class EmailChangeView(LoginRequiredMixin, FormView):
    """
    View displays email change form and sends an verification link to the entered email,
    newly entered email is saved in a temporary variable.
    """
    template_name = 'email_change.html'
    form_class = UserChangeEmailForm
    success_url = reverse_lazy('account:index_account')

    def form_valid(self, form):
        try:
            with transaction.atomic():
                form = self.form_class(self.request.POST, instance=self.request.user)
                form.save()
                self.request.user.temp_email = form.cleaned_data['temp_email']
                self.request.user.save()
                current_site = get_current_site(self.request)
                subject = 'Confirm your new email.'
                # load a template like get_template()
                # and calls its render() method immediately.
                message = render_to_string('activation_request_email.html', {
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(self.request.user.pk)),
                    # method will generate a hash value with user related data
                    'token': account_token_generator.make_token(self.request.user),
                })

                if SEND_EMAILS:
                    send_mail(subject, message, EMAIL_HOST_USER, [str(self.request.user.temp_email)],
                              fail_silently=False)
                else:
                    print(message)

                messages.success(self.request, "Email has been sent. Check your new email for further details.")
                return super().form_valid(form)

        except Exception as e:
            messages.warning(self.request, "Error trying to send email. If problem persists, please contact staff members.")
            return super().form_invalid(form)


def activate_email(request, uidb64, token):
    """
    Function checks token sent during email change process,
    if the token is valid then the new email address is verified and the new email
    is set as the current email, otherwise an error message is shown.
    """
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    # checking if the user exists, if the token is valid.
    if user is not None and account_token_generator.check_token(user, token):

        user.email = user.temp_email
        user.temp_email = None
        user.save()
        login(request, user)
        messages.success(request, "Email has been changed.")
        return redirect('home:home_weekly')
    else:
        messages.warning(request,
                         "Link has expired.")
        return redirect('account:change_email')


class UserLoginView(LoginView):
    form_class = UserLoginForm
    template_name = "login.html"
    success_url = reverse_lazy("account:index_account")


class UserLogoutView(LogoutView):
    pass


class UserPasswordResetView(PasswordResetView):
    template_name = "password_reset.html"
    email_template_name = "password_reset_email.html"
    success_url = reverse_lazy("account:password_reset")
    form_class = UserPasswordResetForm

    def form_valid(self, form):
        messages.success(self.request, "Email has been sent. Check your email for further details.")
        return super().form_valid(form)


class UserPasswordResetConfirmView(PasswordResetConfirmView):
    template_name = "password_reset_confirm.html"
    post_reset_login = True
    success_url = reverse_lazy("account:index_account")
    form_class = UserSetPasswordForm

    def form_valid(self, form):
        messages.success(self.request, "Password has been successfully reset.")
        return super().form_valid(form)


class NotificationReadView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        notification_pk = request.POST.get("notification_pk")
        notification = get_object_or_404(Notification, pk=notification_pk)
        if self.request.user != notification.created_by:
            return HttpResponse(status=403)

        notification.read = True
        notification.save()
        return HttpResponse(status=200)

