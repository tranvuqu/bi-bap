from django.conf.urls import url
from django.urls import path

from bi_bap.account.views import *

app_name = 'account'
urlpatterns = [
    url(r'^$', AccountIndexView.as_view(), name='index_account'),
    url(r'^login/', UserLoginView.as_view(), name='login'),
    url(r'^logout/', UserLogoutView.as_view(), name='logout'),
    url(r'^register/', UserRegisterView.as_view(), name='register'),
    url(r'^password_reset/', UserPasswordResetView.as_view(), name='password_reset'),
    path('password_reset_confirm/<slug:uidb64>/<slug:token>/', UserPasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    url(r'^notification_read/(?P<pk>[\w-]+)/$', NotificationReadView.as_view(), name='notification_read'),
    url(r'^change_password/$', PasswordChangeView.as_view(), name='change_password'),
    path('activate_registration/<slug:uidb64>/<slug:token>/', activate_registration, name='activate_registration'),
    url(r'^profile/change_email/$', EmailChangeView.as_view(), name='change_email'),
    path('activate_email/<slug:uidb64>/<slug:token>/', activate_email, name='activate_email'),
]