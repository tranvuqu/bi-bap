import datetime

from django.test import TestCase

# Create your tests here.
from bi_bap.account.models import User
from bi_bap.activities.models import Activity, ActivityInstance


class UserTestCase(TestCase):
    def setUp(self):
        user1 = User.objects.create(username="user1", level_progress=3)
        Activity.objects.create(name="activity1", created_by=user1,
                                start_date=datetime.datetime(2022, 4, 1),
                                end_date=datetime.datetime(2022, 4, 30))

    def test_evaluate_level_up(self):
        user1 = User.objects.get(username="user1", level_progress=3)
        activity1 = Activity.objects.get(name="activity1")

        complete = ActivityInstance.objects.create(activity=activity1, date=datetime.datetime(2022, 4, 1), is_complete=True)
        user1.evaluate_level_progress(complete)
        self.assertEqual(user1.level_progress, 4)
        self.assertEqual(user1.level_goal, 5)
        self.assertEqual(user1.level, 1)

        user1.evaluate_level_progress(complete)
        self.assertEqual(user1.level_progress, 0)
        self.assertEqual(user1.level_goal, 7)
        self.assertEqual(user1.level, 2)

    def test_evaluate_level_down(self):
        user2 = User.objects.create(username="user2", email="abc@abc.com", level_progress=1, level_goal=7, level=2)
        activity1 = Activity.objects.get(name="activity1")

        not_complete = ActivityInstance.objects.create(activity=activity1, date=datetime.datetime(2022, 4, 2),
                                                       is_complete=False)
        user2.evaluate_level_progress(not_complete)
        self.assertEqual(user2.level_progress, 0)
        self.assertEqual(user2.level_goal, 7)
        self.assertEqual(user2.level, 2)

        user2.evaluate_level_progress(not_complete)
        self.assertEqual(user2.level_progress, 4)
        self.assertEqual(user2.level_goal, 5)
        self.assertEqual(user2.level, 1)
