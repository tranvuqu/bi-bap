from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, PasswordChangeForm, PasswordResetForm, \
    SetPasswordForm
from django.core.exceptions import ValidationError
from django.forms import EmailField, ModelForm

from bi_bap.account.models import User


class UserLoginForm(AuthenticationForm):

    def __init__(self, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)

        self.fields['username'].widget.attrs['placeholder'] = 'Enter username'
        self.fields['password'].widget.attrs['placeholder'] = 'Enter password'

        self.fields['username'].widget.attrs['class'] = 'form-control shadow-none'
        self.fields['password'].widget.attrs['class'] = 'form-control shadow-none'

        self.fields['username'].widget.attrs['autocomplete'] = 'off'
        self.fields['password'].widget.attrs['autocomplete'] = 'off'


class UserRegisterForm(UserCreationForm):
    email = EmailField()

    def __init__(self, *args, **kwargs):
        super(UserRegisterForm, self).__init__(*args, **kwargs)

        self.fields['username'].widget.attrs['placeholder'] = 'Enter username'
        self.fields['email'].widget.attrs['placeholder'] = 'Enter email'
        self.fields['password1'].widget.attrs['placeholder'] = 'Enter password'
        self.fields['password2'].widget.attrs['placeholder'] = 'Enter password again'

        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control shadow-none'

        self.fields['username'].widget.attrs['autocomplete'] = 'off'
        self.fields['password1'].widget.attrs['autocomplete'] = 'off'
        self.fields['password2'].widget.attrs['autocomplete'] = 'off'

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class ChangePasswordForm(PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        for field_name in ('old_password', 'new_password1', 'new_password2'):
            # remove helper text
            self.fields[field_name].help_text = None
            # remove labels
            self.fields[field_name].label = False

            self.fields[field_name].widget.attrs['class'] = 'form-control shadow-none'

        self.fields['old_password'].widget.attrs['placeholder'] = "Current password"
        self.fields['new_password1'].widget.attrs['placeholder'] = "New password"
        self.fields['new_password2'].widget.attrs['placeholder'] = "New password confirmation"

    class Meta:
        model = User
        fields = ['old_password', 'new_password1', 'new_password2']


class UserChangeEmailForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['temp_email'].widget.attrs['placeholder'] = "New email"
        self.fields['temp_email'].label = False
        self.fields['temp_email'].widget.attrs['class'] = 'form-control shadow-none'

    def clean_temp_email(self):
        temp_email = self.cleaned_data.get('temp_email')

        # Check to see if any users already exist with this email as a username.
        try:
            match = User.objects.get(email=temp_email)
        except User.DoesNotExist:
            # Unable to find a user, this is fine
            return temp_email

        # A user was found with this as a username, raise an error.
        raise ValidationError("This email has already been registered.")

    class Meta:
        model = User
        fields = ['temp_email']


class ChangeProfilePicture(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['profile_pic'].widget.attrs['class'] = 'form-control shadow-none'

    class Meta:
        model = User
        fields = ['profile_pic']


class UserPasswordResetForm(PasswordResetForm):
    def __init__(self, *args, **kwargs):
        super(UserPasswordResetForm, self).__init__(*args, **kwargs)

        self.fields['email'].widget.attrs['placeholder'] = 'Enter email address'

        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control shadow-none'

        self.fields['email'].widget.attrs['autocomplete'] = 'off'


class UserSetPasswordForm(SetPasswordForm):
    def __init__(self, *args, **kwargs):
        super(UserSetPasswordForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control shadow-none'

