import os

from PIL import Image
from django.contrib.auth.models import AbstractUser
from django.db import models


def get_upload_path(instance, filename):
    username = instance.username
    return os.path.join("profile_pics", username, filename)


# Create your models here.
class User(AbstractUser):
    email = models.EmailField(unique=True)
    # new email when users change email, temp will be set to email if users authenticate their new email
    temp_email = models.EmailField(null=True, blank=True)
    signup_confirmation = models.BooleanField(default=False)
    profile_pic = models.ImageField(default="default_profile.jpg", upload_to=get_upload_path)
    level = models.IntegerField(verbose_name="Level", default=1)
    level_progress = models.IntegerField(verbose_name="Level progress", default=0)
    level_goal = models.IntegerField(verbose_name="Level goal", default=5)

    def calculate_progress(self):
        return round(self.level_progress / (self.level_goal / 100))

    def evaluate_level_progress(self, activity_instance):
        next_level_increment = 2
        if activity_instance.is_complete:
            self.level_progress += 1
            if self.level_progress == self.level_goal:
                self.level += 1
                self.level_progress = 0
                self.level_goal += next_level_increment
        else:
            if not (self.level_progress == 0 and self.level == 1):
                self.level_progress -= 1
                if self.level_progress == -1:
                    self.level -= 1
                    self.level_goal -= next_level_increment
                    self.level_progress = self.level_goal - 1
        self.save()

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        if self.profile_pic:
            img = Image.open(self.profile_pic.path)

            if img.height > 300 or img.width > 300:
                output_size = (300, 300)
                img.thumbnail(output_size)
                img.save(self.profile_pic.path)
            else:
                output_size = (300, 300)
                img.thumbnail(output_size)
                img.save(self.profile_pic.path)


class Notification(models.Model):
    text = models.CharField(max_length=100)
    created_by = models.ForeignKey(User, verbose_name="User", on_delete=models.CASCADE, related_name="notifications")
    read = models.BooleanField(default=False)

    def __str__(self):
        return self.text

