   function disable_weekdays() {
            $('#id_monday').prop("disabled", true);
            $('#id_tuesday').prop("disabled", true);
            $('#id_wednesday').prop("disabled", true);
            $('#id_thursday').prop("disabled", true);
            $('#id_friday').prop("disabled", true);
            $('#id_saturday').prop("disabled", true);
            $('#id_sunday').prop("disabled", true);
    }
    function enable_weekdays() {
       $('#id_monday').prop("disabled", true);
            $('#id_monday').prop("disabled", false);
            $('#id_tuesday').prop("disabled", false);
            $('#id_wednesday').prop("disabled", false);
            $('#id_thursday').prop("disabled", false);
            $('#id_friday').prop("disabled", false);
            $('#id_saturday').prop("disabled", false);
            $('#id_sunday').prop("disabled", false);
    }
    $('select[name=action]').change(function(){
        if ($('#id_action :selected').val() == "shorten"){
           disable_weekdays();
         }
         else{
           enable_weekdays();
         }
    });

    $('#activity_edit').submit(function() {
            enable_weekdays();
    })

    $('document').ready(function(){
        if ($('#id_action :selected').val() == "shorten"){
            disable_weekdays();
        }
        else{
            enable_weekdays();
        }
    });