if ($(window).width() < 1000) {
     collapse_sidebar()
}
else {
    if(sessionStorage && sessionStorage.getItem("collapsed") == "true" )
        collapse_sidebar()
     else
        sessionStorage.setItem("collapsed", "false");
}

function boolean_string_negation(val){
    if(val == "true"){
        return "false"
    }
    else{
        return "true"
    }
}

function collapse_sidebar(){
    $("#collapsed-sidebar").toggleClass("d-none")
    $("#open-sidebar").toggleClass("d-none")
    $(".base-wrapper  .sidebar-wrapper").toggleClass("sidebar-collapsed")
    $(".hamburger-container").toggleClass("hamburger-container-collapsed")
    $(".top-bar").toggleClass("top-bar-collapsed")
    $(".side-content").toggleClass("side-content-collapsed")
    $(".main-content").toggleClass("main-content-collapsed")
    $(".base-wrapper  .sidebar").toggleClass("sidebar-collapsed")


    if( ! $(".base-wrapper .sidebar").hasClass("sidebar-collapsed")){
       setTimeout(function(){
                $(".base-wrapper  .hidden").toggleClass("invisible")
        }, 200);
    }else{
                $(".base-wrapper  .hidden").toggleClass("invisible")
    }
}

$("#collapse-sidebar").click(function() {
    collapse_sidebar();
    sessionStorage.setItem("collapsed", boolean_string_negation(sessionStorage.getItem("collapsed")));
});

const activities = document.querySelectorAll(".activity")

const observer = new IntersectionObserver(
    entries => {
        entries.forEach(entry => {
            entry.target.classList.toggle("show", entry.isIntersecting)
        })
        },
        {
            threshold: 0.6,
        }

)

activities.forEach(activity =>{
    observer.observe(activity)
})


$(".activity-submit").click(function() {
    var metadata = $(this).closest(".sidebar-activity").find(".activity-metadata")
    var activity_pk = metadata.find("input[name='activity_pk']").val()
    var csrf_token = metadata.find("input[name='csrfmiddlewaretoken']").val()
    var update_activity_url = metadata.find("input[name='update_activity_url']").val()
    var complete = $(this).closest(".sidebar-activity").find(".activity-checkbox").is(":checked")
    var start_difficulty = $(this).closest(".sidebar-activity").find(".start-difficulty-select option:selected").val()
    var activity_difficulty = $(this).closest(".sidebar-activity").find(".activity-difficulty-select option:selected").val()

    $.ajax({
        method: "POST",
        url: update_activity_url,
        data: {
            csrfmiddlewaretoken: csrf_token,
            activity_pk: activity_pk,
            start_difficulty: start_difficulty,
            activity_difficulty: activity_difficulty,
            complete: complete,
        },
        success: function() {
            window.location.reload();
        },
        error: function(xhr){
            $( "#messages" ).append('<div class="alert alert-danger text-center align-center h6" id="hideMe">Error processing request.</div>');
        }
    })
})

$(".activity-table-submit").click(function() {
    var metadata = $(this).closest(".activity-table-cell").find(".activity-table-metadata")
    var activity_pk = metadata.find("input[name='activity_pk']").val()
    var csrf_token = metadata.find("input[name='csrfmiddlewaretoken']").val()
    var update_activity_url = metadata.find("input[name='update_activity_url']").val()
    var complete = $(this).closest(".activity-table-cell").find(".activity-checkbox").is(":checked")
    var start_difficulty = $(this).closest(".activity-table-cell").find(".start-difficulty-select option:selected").val()
    var activity_difficulty = $(this).closest(".activity-table-cell").find(".activity-difficulty-select option:selected").val()

    $.ajax({
        method: "POST",
        url: update_activity_url,
        data: {
            csrfmiddlewaretoken: csrf_token,
            activity_pk: activity_pk,
            start_difficulty: start_difficulty,
            activity_difficulty: activity_difficulty,
            complete: complete,
        },
        success: function() {
            sessionStorage.scrollTop = document.body.scrollTop;
            window.location.reload();
        },
        error: function(xhr){
            $( "#messages" ).append('<div class="alert alert-danger text-center align-center h6" id="hideMe">Error processing request.</div>');
        }
    })
})

$(".notification").click(function() {
    var data       = $(this).closest(".notification").find(".notification-metadata")
    var notification_pk       = data.find("input[name='notification_pk']").val()
    var token      = data.find("input[name='csrfmiddlewaretoken']").val()
    var notification_url = data.find("input[name='notification_url']").val()

            $.ajax({
                method: "POST",
                url: notification_url,
                data: {
                    csrfmiddlewaretoken: token,
                    notification_pk: notification_pk,
                },
                success: function() {
                    $('#' + notification_pk).remove();
                    var text = $('#notification-number').text()
                    $('#notification-number').text(text - 1);
                },
                error: function(xhr) {

                }
            })

})

$(document).ready(function() {
  if (sessionStorage.scrollTop != "undefined") {
    $(document.body).scrollTop(sessionStorage.scrollTop);
    sessionStorage.scrollTop = 0;
  }
});