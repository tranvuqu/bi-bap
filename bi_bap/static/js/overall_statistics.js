backgroundColors = [
      'rgba(255, 99, 132, 0.2)',
      'rgba(255, 159, 64, 0.2)',
      'rgba(255, 205, 86, 0.2)',
      'rgba(75, 192, 192, 0.2)',
      'rgba(54, 162, 235, 0.2)',
      'rgba(153, 102, 255, 0.2)',
      'rgba(201, 203, 207, 0.2)',
      'rgba(201, 159, 27, 0.2)',
      'rgba(99, 175, 189, 0.2)',
      'rgba(162, 66, 91, 0.2)',
      'rgba(15, 64, 189, 0.2)'
    ]
borderColors = [
      'rgb(255, 99, 132)',
      'rgb(255, 159, 64)',
      'rgb(255, 205, 86)',
      'rgb(75, 192, 192)',
      'rgb(54, 162, 235)',
      'rgb(153, 102, 255)',
      'rgb(201, 203, 207)',
      'rgb(201, 159, 27)',
      'rgb(99, 175, 189)',
      'rgb(162, 66, 91)',
      'rgb(15, 64, 189)'
    ]

ctx = document.getElementById('day_dict_chart').getContext('2d');
myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: day_dict_labels,
        datasets: [{
            label: 'Success rate',
            data: day_dict_values,
            backgroundColor: [
                'rgba(50, 205, 50, 0.2)'
            ],
            borderColor: [
                'rgba(50, 205, 50, 1)'
            ],
            fill: false,
            tension: 0.1,
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true,
                suggestedMax: 100,
                ticks: {
                    // Include a dollar sign in the ticks
                    callback: function(value, index, ticks) {
                        return value + '%';
                    }
                }
            }
        }
    }
});

ctx = document.getElementById('activity_dict_chart').getContext('2d');
myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: activity_dict_labels,
        datasets: [{
            label: 'Success rate by activity',
            data: activity_dict_values,
            backgroundColor: backgroundColors,
            borderColor: borderColors,
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true,
                suggestedMax: 100,
                ticks: {
                    // Include a dollar sign in the ticks
                    callback: function(value, index, ticks) {
                        return value + '%';
                    }
                }
            }
        }
    }
});

ctx = document.getElementById('activity_composition_dict_chart').getContext('2d');
myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: activity_composition_dict_labels,
        datasets: [{
            label: 'Activity composition',
            data: activity_composition_dict_values,
            backgroundColor: backgroundColors,
            borderColor: borderColors,
            hoverOffset: 4
        }]
    },
});

ctx = document.getElementById('activity_start_difficulty_dict_chart').getContext('2d');
myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: start_activity_difficulty_dict_labels,
        datasets: [{
            label: 'Average starting difficulty',
            data: start_activity_difficulty_dict_values,
            backgroundColor: backgroundColors,
            borderColor: borderColors,
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                suggestedMax: 5,
                beginAtZero: true
            }
        }
    }
});

ctx = document.getElementById('activity_difficulty_dict_chart').getContext('2d');
myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: activity_difficulty_dict_labels,
        datasets: [{
            label: 'Average difficulty to complete an activity',
            data: activity_difficulty_dict_values,
            backgroundColor: backgroundColors,
            borderColor: borderColors,
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                suggestedMax: 5,
                beginAtZero: true,
            }
        }
    }
});
