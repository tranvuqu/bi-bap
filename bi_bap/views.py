from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views.generic import ListView, TemplateView, FormView

from bi_bap.account.forms import UserRegisterForm
from bi_bap.account.views import UserRegisterView


class IndexView(UserRegisterView):
    form_class = UserRegisterForm
    template_name = "index_signup.html"
    success_url = reverse_lazy('index')
