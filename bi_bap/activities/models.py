from django.db import models

# Create your models here.
from bi_bap.account.models import User

SCALE = (
    (1, "Very easy"),
    (2, "Easy"),
    (3, "Neutral"),
    (4, "Hard"),
    (5, "Very hard"),
)

class Activity(models.Model):
    name = models.CharField("Name", max_length=20)
    created_by = models.ForeignKey(User, verbose_name="User", on_delete=models.CASCADE, related_name="activities")
    start_date = models.DateField("Start date")
    end_date = models.DateField("End date")
    monday = models.BooleanField(default=False, blank=True)
    tuesday = models.BooleanField(default=False, blank=True)
    wednesday = models.BooleanField(default=False, blank=True)
    thursday = models.BooleanField(default=False, blank=True)
    friday = models.BooleanField(default=False, blank=True)
    saturday = models.BooleanField(default=False, blank=True)
    sunday = models.BooleanField(default=False, blank=True)

    def __str__(self):
        return str(self.name)

    def get_interval(self):
        return self.start_date.strftime("%d/%m/%Y") + " - " + self.end_date.strftime("%d/%m/%Y")


class ActivityInstance(models.Model):
    activity = models.ForeignKey(Activity, verbose_name="Activity", on_delete=models.CASCADE, related_name="activity_instances")
    is_complete = models.BooleanField("Complete", default=False)
    start_difficulty = models.IntegerField(default=1, choices=SCALE)
    activity_difficulty = models.IntegerField(default=1, choices=SCALE)
    date = models.DateField("Date")

    def __str__(self):
        return str(self.activity.name) + ": " + str(self.date)