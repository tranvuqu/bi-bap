from django.conf.urls import url

from bi_bap.activities.views import *

app_name = 'activities'
urlpatterns = [
    url(r'^$', ActivitiesIndexView.as_view(), name='index_activities'),
    url(r'^create_activity/$', CreateActivityView.as_view(), name='create_activity'),
    url(r'^delete_activity/(?P<pk>[\w-]+)/$', DeleteActivityView.as_view(), name='delete_activity'),
    url(r'^update_activity/(?P<pk>[\w-]+)/$', UpdateActivityView.as_view(), name='update_activity'),
    url(r'^activity_statistics/(?P<pk>[\w-]+)/$', ActivityStatisticsView.as_view(), name='activity_statistics'),
    url(r'^update_activity_instance/(?P<pk>[\w-]+)/$', UpdateActivityInstanceView.as_view(), name='update_activity_instance'),

]