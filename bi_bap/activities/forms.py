from django.core.exceptions import ValidationError
from django.forms import ModelForm, BooleanField, ChoiceField, RadioSelect
from bootstrap_datepicker_plus.widgets import DatePickerInput
from bi_bap.activities.models import Activity
from bi_bap.settings import DATE_INPUT_FORMAT

activity_edit_choices = [('extend', 'Extend'),
                         ('shorten', 'Shorten'),
                         ('generate again', 'Generate again')]


class ActivityForm(ModelForm):
    class Meta:
        model = Activity
        fields = ['name', 'start_date', 'end_date', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday',
                  'sunday']
        widgets = {
            'start_date': DatePickerInput(format=DATE_INPUT_FORMAT),
            'end_date': DatePickerInput(format=DATE_INPUT_FORMAT),
        }

    def __init__(self, *args, **kwargs):
        super(ActivityForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            if field in ['name', 'start_date', 'end_date']:
                self.fields[field].widget.attrs['class'] = 'form-control shadow-none'
            else:
                self.fields[field].widget.attrs['class'] = 'form-check-input'

    def clean(self):
        cleaned_data = super(ActivityForm, self).clean()

        if "start_date" not in cleaned_data or "end_date" not in cleaned_data:
            raise ValidationError("Invalid date format.")

        if cleaned_data['start_date'] >= cleaned_data['end_date']:
            raise ValidationError("Invalid date range.")

        if not cleaned_data['monday'] and not cleaned_data['tuesday'] and not cleaned_data['wednesday'] \
                and not cleaned_data['thursday'] and not cleaned_data['friday'] \
                and not cleaned_data['saturday'] and not cleaned_data['sunday']:
            raise ValidationError("Choose at least one weekday.")
        return cleaned_data


class ActivityEditForm(ActivityForm):
    action = ChoiceField(choices=activity_edit_choices)
    old_instance_start_date = None
    old_instance_end_date = None

    def __init__(self, *args, **kwargs):
        super(ActivityEditForm, self).__init__(*args, **kwargs)
        self.old_instance_start_date = self.instance.start_date
        self.old_instance_end_date = self.instance.end_date
        self.fields['action'].widget.attrs['class'] = 'form-select'

    def clean(self):
        cleaned_data = super(ActivityEditForm, self).clean()

        if self.instance.start_date and cleaned_data['action'] == 'extend' and cleaned_data['start_date'] > self.instance.start_date:
            raise ValidationError("Start date cannot be higher than previous start date.")

        if self.instance.end_date and cleaned_data['action'] == 'extend' and cleaned_data['end_date'] < self.instance.end_date:
            raise ValidationError("End date cannot be lower than previous end date.")

        if self.instance.start_date and cleaned_data['action'] == 'shorten' and cleaned_data['start_date'] < self.instance.start_date:
            raise ValidationError("Start date cannot be lower than previous start date.")

        if self.instance.end_date and cleaned_data['action'] == 'shorten' and cleaned_data['end_date'] > self.instance.end_date:
            raise ValidationError("End date cannot be higher previous end date.")




