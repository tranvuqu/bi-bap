from django.contrib import admin

# Register your models here.
from bi_bap.activities.models import Activity, ActivityInstance

admin.site.register(Activity)
admin.site.register(ActivityInstance)
