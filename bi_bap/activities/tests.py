import datetime

from django.test import TestCase

# Create your tests here.
from bi_bap.account.models import User
from bi_bap.activities.models import Activity
from bi_bap.activities.views import generate_activity_by_weekday


class GenerateActivityTestCase(TestCase):
    start_date = datetime.datetime(2022, 4, 1)
    end_date = datetime.datetime(2022, 4, 30)

    def setUp(self):
        user1 = User.objects.create(username="user1")
        Activity.objects.create(name="activity1", created_by=user1,
                                start_date= self.start_date,
                                end_date=self.end_date)

    def test_generate_activity_by_monday(self):
        activity1 = Activity.objects.get(name="activity1")
        target_weekday = 1
        activity_instances = generate_activity_by_weekday(self.start_date, self.end_date, activity1, target_weekday)
        self.assertEqual(len(activity_instances), 4)
        self.assertEqual(activity_instances[0].date.isoweekday(), target_weekday)
        self.assertEqual(activity_instances[0].date.day, 4)
        self.assertEqual(activity_instances[1].date.day, 11)
        self.assertEqual(activity_instances[2].date.day, 18)
        self.assertEqual(activity_instances[3].date.day, 25)

    def test_generate_activity_by_saturday(self):
        activity1 = Activity.objects.get(name="activity1")
        target_weekday = 6
        activity_instances = generate_activity_by_weekday(self.start_date, self.end_date, activity1, target_weekday)
        self.assertEqual(len(activity_instances), 5)
        self.assertEqual(activity_instances[0].date.isoweekday(), target_weekday)
        self.assertEqual(activity_instances[0].date.day, 2)
        self.assertEqual(activity_instances[1].date.day, 9)
        self.assertEqual(activity_instances[2].date.day, 16)
        self.assertEqual(activity_instances[3].date.day, 23)
        self.assertEqual(activity_instances[4].date.day, 30)