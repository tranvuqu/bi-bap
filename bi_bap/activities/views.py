from datetime import timedelta, datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView, UpdateView, DeleteView
from django.views.generic.base import View

from bi_bap.achievements.models import Achievement
from bi_bap.achievements.views import create_num_of_completion_achievement, create_streak_achievement
from bi_bap.activities.forms import ActivityForm, ActivityEditForm
from bi_bap.activities.models import Activity, ActivityInstance
from bi_bap.activity_statistics.views import get_activity_statistics


class ActivitiesIndexView(LoginRequiredMixin, TemplateView):
    template_name = "index_activities.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["current_activities"] = Activity.objects.filter(created_by=self.request.user).order_by("name")
        return context


def generate_activity_by_weekday(start_date, end_date, activity, target_weekday):
    """
    Generates activity instances for a weekday in a given date interval.
    """
    date_offset = target_weekday - start_date.isoweekday()
    if date_offset < 0:
        date = start_date + timedelta(7 + date_offset)
    else:
        date = start_date + timedelta(date_offset)
    activities = []
    while date <= end_date:
        activities.append(ActivityInstance(activity=activity, date=date))
        date = date + timedelta(7)

    return activities


def generate_activity(start_date, end_date, activity, *days):
    new_activities = []
    if len(days) != 7:
        return new_activities

    target_weekday = 1
    for day in days:
        if day is True:
            new_activities += generate_activity_by_weekday(start_date, end_date, activity, target_weekday)
        target_weekday += 1

    return new_activities


class CreateActivityView(LoginRequiredMixin, CreateView):
    form_class = ActivityForm
    template_name = "create_activity.html"
    success_url = reverse_lazy('activities:index_activities')

    def form_valid(self, form):
        new_activity = form.save(commit=False)
        new_activity.created_by = self.request.user
        cleaned_data = form.cleaned_data
        start_date = cleaned_data['start_date']
        end_date = cleaned_data['end_date']

        new_activities = generate_activity(start_date, end_date, new_activity,
                                           cleaned_data['monday'], cleaned_data['tuesday'], cleaned_data['wednesday'],
                                           cleaned_data['thursday'], cleaned_data['friday'], cleaned_data['saturday'],
                                           cleaned_data['sunday'])

        new_activity.save()

        if new_activities:
            new_achievements = []
            total_activities = len(new_activities)
            new_achievements += create_num_of_completion_achievement(new_activity, self.request.user, 4, total_activities)
            new_achievements += create_streak_achievement(new_activity, self.request.user, 50, total_activities)
            ActivityInstance.objects.bulk_create(new_activities)
            Achievement.objects.bulk_create(new_achievements)

        return super(CreateActivityView, self).form_valid(form)


class UpdateActivityView(LoginRequiredMixin, UpdateView):
    model = Activity
    form_class = ActivityEditForm
    template_name = "update_activity.html"
    success_url = reverse_lazy('activities:index_activities')

    def form_valid(self, form):
        cleaned_data = form.cleaned_data
        start_date = cleaned_data['start_date']
        end_date = cleaned_data['end_date']
        activities_to_create = []
        new_activities = generate_activity(start_date, end_date, form.instance,
                                           cleaned_data['monday'],
                                           cleaned_data['tuesday'],
                                           cleaned_data['wednesday'],
                                           cleaned_data['thursday'],
                                           cleaned_data['friday'],
                                           cleaned_data['saturday'],
                                           cleaned_data['sunday'])

        if cleaned_data['action'] == 'extend':
            for new_activity in new_activities:
                if new_activity.date < form.old_instance_start_date or new_activity.date > form.old_instance_end_date:
                    activities_to_create.append(new_activity)

        elif cleaned_data['action'] == 'shorten':
            ActivityInstance.objects.filter(activity=form.instance).exclude(date__gte=start_date, date__lte=end_date).delete()
        else:
            ActivityInstance.objects.filter(activity=form.instance).delete()
            activities_to_create = new_activities

        if activities_to_create:
            ActivityInstance.objects.bulk_create(activities_to_create)

        return super(UpdateActivityView, self).form_valid(form)


class DeleteActivityView(LoginRequiredMixin, DeleteView):
    model = Activity
    success_url = reverse_lazy('activities:index_activities')

    def post(self, request, *args, **kwargs):
        activity = get_object_or_404(Activity, pk=self.kwargs['pk'])
        if self.request.user != activity.created_by:
            return HttpResponse(status=403)

        return super(DeleteActivityView, self).post(request)


class UpdateActivityInstanceView(LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        try:
            activity_pk = request.POST.get("activity_pk")
            complete = request.POST.get("complete")
            start_difficulty = request.POST.get("start_difficulty")
            activity_difficulty = request.POST.get("activity_difficulty")
        except KeyError:
            return HttpResponse(status=403)

        activity_instance = get_object_or_404(ActivityInstance, pk=activity_pk)
        if self.request.user != activity_instance.activity.created_by:
            return HttpResponse(status=403)

        if complete == "true":
            new_activity_state = True
        elif complete == "false":
            new_activity_state = False
        else:
            return HttpResponse(status=403)
        old_activity_state = activity_instance.is_complete
        activity_instance.is_complete = new_activity_state
        activity_instance.start_difficulty = start_difficulty
        activity_instance.activity_difficulty = activity_difficulty
        activity_instance.save()

        if old_activity_state != new_activity_state:
            self.request.user.evaluate_level_progress(activity_instance)
            achievements = activity_instance.activity.achievements.all()
            for achievement in achievements:
                achievement.evaluate_achievement(activity_instance)

        return HttpResponse(status=200)


class ActivityStatisticsView(LoginRequiredMixin, TemplateView):
    template_name = "activity_statistics.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            date_format = "%A"
            current_activity = get_object_or_404(Activity, pk=self.kwargs['pk'], created_by=self.request.user)
            day_dict, start_activity_difficulty_dict, activity_difficulty_dict = get_activity_statistics(self.request.user, date_format, activity=current_activity)
            context['current_activity'] = current_activity
            context['day_dict'] = day_dict
            context['start_activity_difficulty_dict'] = start_activity_difficulty_dict
            context['activity_difficulty_dict'] = activity_difficulty_dict
            return context
        except KeyError:
            return context
        except Activity.DoesNotExist:
            return context
