from datetime import date

from bi_bap.account.models import Notification
from bi_bap.activities.models import ActivityInstance


def add_activities_to_context(request):
    if request.user.is_authenticated:
        instances = ActivityInstance.objects.filter(activity__created_by=request.user, date=date.today()).order_by("is_complete", "activity__name")
        notifications = Notification.objects.filter(created_by=request.user, read=False)
        return {
            'today_activities': instances,
            'notifications': notifications,
        }
    else:
        return ""
