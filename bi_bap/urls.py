"""bi_bap URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static

from bi_bap import settings
from bi_bap.views import IndexView

urlpatterns = [
    path('admin/', admin.site.urls),

    url(r'^$', IndexView.as_view(), name='index'),
    # url(r'^login/$', account_view.LoginView.as_view(), name='login'),
    # url(r'^logout/$', account_view.LogoutView.as_view(), name='logout'),

    # apps
    url(r'^account/', include('bi_bap.account.urls')),
    url(r'^achievements/', include('bi_bap.achievements.urls')),
    url(r'^activities/', include('bi_bap.activities.urls')),
    url(r'^statistics/', include('bi_bap.activity_statistics.urls')),
    url(r'^rewards/', include('bi_bap.rewards.urls')),
    url(r'^home/', include('bi_bap.home.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
