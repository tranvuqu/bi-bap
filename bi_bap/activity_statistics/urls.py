from django.conf.urls import url
from bi_bap.activity_statistics.views import *


app_name = 'activity_statistics'
urlpatterns = [
    url(r'^monthly/$', OverallMonthlyStatisticsView.as_view(), name='overall_monthly_statistics'),
    url(r'^yearly/$', OverallYearlyStatisticsView.as_view(), name='overall_yearly_statistics'),
    url(r'^all_time/$', OverallAllTimeStatisticsView.as_view(), name='overall_all_time_statistics'),

]