from django.apps import AppConfig


class ActivityStatisticsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'activity_statistics'
