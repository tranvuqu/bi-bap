import calendar
from datetime import date, datetime

from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.
from django.views.generic import TemplateView

from bi_bap.activities.models import ActivityInstance
from bi_bap.settings import DATE_INPUT_FORMAT


class Statistics:
    completed = 0
    total = 0

    def __str__(self):
        return str(self.completed) + " " + str(self.total)

    def calculate_percentage(self):
        if self.total == 0 or self.completed == 0:
            return 0
        else:
            return round(self.completed / (self.total / 100))

    def calculate_average(self):
        if self.total == 0 or self.completed == 0:
            return 0
        else:
            return round(self.completed / self.total, 1)


def get_activity_statistics(user, date_format, activity):
    """
    Function creates data dictionaries for graphs to display statistics of an activity.
    """
    date_today = datetime.today()
    instances = list(ActivityInstance.objects.filter(activity__created_by=user, activity=activity, date__lte=date_today)
                                             .order_by("date"))

    day_dict = get_statistics_by_day(date_format, instances)
    start_activity_difficulty_dict = get_average_activity_start_difficulty(instances)
    activity_difficulty_dict = get_average_activity_difficulty(instances)
    return day_dict, start_activity_difficulty_dict, activity_difficulty_dict


def get_overall_statistics(user, date_format, start_date=None, end_date=None):
    """
    Function creates data dictionaries for graphs to display overall statistics in the chosen date interval.
    """
    date_today = datetime.today()
    if not (start_date and end_date):
        instances = list(ActivityInstance.objects.filter(activity__created_by=user,
                                                         date__lte=date_today)
                                                 .order_by("date"))

    elif date_today <= end_date:
        instances = list(ActivityInstance.objects.filter(date__gte=start_date,
                                                         date__lte=date_today,
                                                         activity__created_by=user)
                                                 .order_by("date"))
    else:
        instances = list(ActivityInstance.objects.filter(date__gte=start_date,
                                                         date__lte=end_date,
                                                         activity__created_by=user)
                                                 .order_by("date"))

    day_dict = get_statistics_by_day(date_format, instances)
    activity_dict = get_statistics_by_activities(instances)
    activity_composition_dict = get_activity_composition(instances)
    start_activity_difficulty_dict = get_average_activity_start_difficulty(instances)
    activity_difficulty_dict = get_average_activity_difficulty(instances)

    return day_dict, activity_dict, activity_composition_dict, start_activity_difficulty_dict, activity_difficulty_dict


def get_statistics_by_day(date_format, activity_instances):
    """
    Function creates data dictionary for a graph, which shows success rate of activities.
    Data on x axis are determined by date_format argument.
    Data on y axis are calculated by Statistics instances.
    """
    day_dict = {}
    for instance in activity_instances:
        instance_format = instance.date.strftime(date_format)

        if not day_dict.get(instance_format):
            day_dict[instance_format] = Statistics()

        if instance.is_complete:
            day_dict[instance_format].completed += 1

        day_dict[instance_format].total += 1

    return day_dict


def get_statistics_by_activities(activity_instances):
    """
    Function creates data dictionary for a graph, which shows the success rate of each activity.
    Data on x axis are determined by unique activities.
    Data on y axis are calculated by Statistics instances.
    """
    activity_dict = {}
    for instance in activity_instances:
        activity = instance.activity

        if not activity_dict.get(activity):
            activity_dict[activity] = Statistics()

        if instance.is_complete:
            activity_dict[activity].completed += 1

        activity_dict[activity].total += 1

    return activity_dict


def get_activity_composition(activity_instances):
    """
    Function creates data dictionary for a graph, which shows the composition of activities in the chosen date interval.
    Data on x axis are determined by unique activities.
    Data on y axis are calculated by the number of activity instances.
    """
    activity_composition_dict = {}
    for instance in activity_instances:
        activity = instance.activity

        if not activity_composition_dict.get(activity):
            activity_composition_dict[activity] = 0

        activity_composition_dict[activity] += 1

    return activity_composition_dict


def get_average_activity_start_difficulty(activity_instances):
    """
    Function creates data dictionary for a graph, which shows the average difficulty to start of each activity.
    Data on x axis are determined by unique activities.
    Data on y axis are calculated by Statistics instances.
    """
    start_activity_difficulty_dict = {}
    for instance in activity_instances:
        activity = instance.activity

        if not start_activity_difficulty_dict.get(activity):
            start_activity_difficulty_dict[activity] = Statistics()

        if instance.is_complete:
            start_activity_difficulty_dict[activity].completed += instance.start_difficulty
            start_activity_difficulty_dict[activity].total += 1

    return start_activity_difficulty_dict


def get_average_activity_difficulty(activity_instances):
    """
    Function creates data dictionary for a graph, which shows the average difficulty to complete of each activity.
    Data on x axis are determined by unique activities.
    Data on y axis are calculated by Statistics instances.
    """
    activity_difficulty_dict = {}
    for instance in activity_instances:
        activity = instance.activity

        if not activity_difficulty_dict.get(activity):
            activity_difficulty_dict[activity] = Statistics()

        if instance.is_complete:
            activity_difficulty_dict[activity].completed += instance.activity_difficulty
            activity_difficulty_dict[activity].total += 1

    return activity_difficulty_dict


class OverallMonthlyStatisticsView(LoginRequiredMixin, TemplateView):
    template_name = "overall_monthly_statistics.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        date_format = DATE_INPUT_FORMAT
        if not ("date" in self.request.GET):
            date_to_show = date.today()
        else:
            try:
                date_to_show = datetime.strptime(str(self.request.GET["date"]), date_format)
            except:
                date_to_show = date.today()

        last_day = calendar.monthrange(date_to_show.year, date_to_show.month)[1]
        date_from = datetime(date_to_show.year, date_to_show.month, 1)
        date_to = datetime(date_to_show.year, date_to_show.month, last_day)
        day_dict, activity_dict, activity_composition_dict, start_activity_difficulty_dict, activity_difficulty_dict \
            = get_overall_statistics(self.request.user, date_format, date_from, date_to)

        context['date_to_show'] = date_to_show
        context['day_dict'] = day_dict
        context['activity_dict'] = activity_dict
        context['activity_composition_dict'] = activity_composition_dict
        context['start_activity_difficulty_dict'] = start_activity_difficulty_dict
        context['activity_difficulty_dict'] = activity_difficulty_dict
        return context


class OverallYearlyStatisticsView(LoginRequiredMixin, TemplateView):
    template_name = "overall_yearly_statistics.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        date_format = "%m, %Y"
        if not ("date" in self.request.GET):
            date_to_show = date.today()
        else:
            try:
                date_to_show = datetime.strptime(str(self.request.GET["date"]), DATE_INPUT_FORMAT)
            except:
                date_to_show = date.today()

        date_from = datetime(date_to_show.year, 1, 1)
        date_to = datetime(date_to_show.year, 12, 31)
        day_dict, activity_dict, activity_composition_dict, start_activity_difficulty_dict, activity_difficulty_dict \
            = get_overall_statistics(self.request.user, date_format, date_from, date_to)

        context['date_to_show'] = date_to_show
        context['day_dict'] = day_dict
        context['activity_dict'] = activity_dict
        context['activity_composition_dict'] = activity_composition_dict
        context['start_activity_difficulty_dict'] = start_activity_difficulty_dict
        context['activity_difficulty_dict'] = activity_difficulty_dict
        return context


class OverallAllTimeStatisticsView(LoginRequiredMixin, TemplateView):
    template_name = "overall_all_time_statistics.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        date_format = "%m, %Y"
        day_dict, activity_dict, activity_composition_dict, start_activity_difficulty_dict, activity_difficulty_dict \
            = get_overall_statistics(self.request.user, date_format)

        context['day_dict'] = day_dict
        context['activity_dict'] = activity_dict
        context['activity_composition_dict'] = activity_composition_dict
        context['start_activity_difficulty_dict'] = start_activity_difficulty_dict
        context['activity_difficulty_dict'] = activity_difficulty_dict
        return context
